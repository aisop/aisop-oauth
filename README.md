# Use Maharas OAuth 1.0a in your Node.js app.

A sample application that demonstrates how to use Mahara's OAuth 1.0a API to obtain an access token and sign a RESTful web service request.

## Background

This repository was created as part of the larger project '[aisop-webapp](https://gitlab.com/aisop/aisop-webapp)'. It only provides an excerpt of its functionality, namely the use of the __OAuth 1.0 interface of Mahara__. This is used in the aisop-webapp to download student e-portfolios and analyze them using artificial intelligence. To access the e-portfolios, the students have to authorize the aisop-webapp in an OAuth 1.0a dance. After that, Maharas RESTful web services can be used in combination with OAuth 1.0a. Mahara provides [sample PHP code](https://wiki.mahara.org/wiki/Plugins/Auth/WebServices#REST_with_OAuth_authentication) that demonstrates the use of OAuth 1.0a. This code communicates nicely how the OAuth 1.0a dance works, but uses [a library from 2010](https://code.google.com/archive/p/oauth-php/). So if you want to use Mahara's web services with OAuth 1.0a from a Node.js environment, this repository is for you.

## Requirements and preparations in Mahara

- Node.js must be installed. This app was tested with Node 16 (v16.13.2).
- An understanding of the OAuth 1.0a dance. See the [OAuth 1.0 Protocol](https://www.rfc-editor.org/rfc/rfc5849) and [OAuth Core 1.0 Revision A](https://oauth.net/core/1.0a/)
- A running Mahara installation: The minimum version is [15.04.](https://manual.mahara.org/en/15.04/administration/web_services.html) which introduces Web Services. However, this project was tested with version [22.10](https://manual.mahara.org/de/22.10/). You also need admin rights on your Mahara installation. Please make sure it commincates via HTTPS (SSL/TLS).
- Web service functionality: Enable incoming web service requests (master switch) and both OAuth and REST protocols in Mahara. Find it in the admin panel under *Web Services > Configuration*. For details, see the [Mahara documentation](https://manual.mahara.org/en/22.10/administration/web_services.html).
- Use an existing 'service group' or possibly better: create a new [service group](https://manual.mahara.org/en/22.10/administration/web_services.html#index-5) for your needs. Simply said, service groups are a collection of the webservices you want to use. In the case of this app you have to add at least the function called 'mahara_user_get_extended_context' by which you can receive details about your authenticated user.
- Register an external app: In Mahara, you have to [register external applications](https://manual.mahara.org/en/22.10/administration/web_services.html#registration-of-external-apps) that are connected via OAuth. While creation, give it a name and choose the service group you have previously generated. When editing the external app, you can copy the 'Consumer key' and 'Consumer secret' which will be relevant later. Please make sure you correctly set the Callback URI to 'http://localhost:3000/oauth' (or the domain you're running this app on). This is the URI, the users will be redirected from Mahara to your application after they gave authorization. 

## How to run with Node.js

- Clone this repository using `git clone git@gitlab.com:aisop/aisop-oauth.git`
- Open routes/oauth.js and fill in the following details
	- OAUTH_CONSUMER_KEY of your external app
	- OAUTH_CONSUMER_SECRET of your external app
	- Replace 'your-domain.com' in the endpoints section with the domain your are running Mahara on.
- Run `npm install` to install missing modules
- Run `npm start`
- In your browser, navigate to the oauth-route to start the process e.g. http://localhost:3000/oauth
- Follow the logs in your terminal or webserver.
- Please be prepared to copy and paste the 'oauth_token_secret' in the console. Read the section 'Your next steps' for an explanation.

## Your next steps

When you start the app in the console, you will be asked to enter the 'oauth_token_secret' in its course. Just scroll up a few lines, copy and paste it as command line input. Of course, this is not intended for use in a live system but only for demonstration purposes. You must now include a database to temporarily store this secret between the server-requests. Furthermore, you have to store the final returned Access Token together with the secret in the database. You can inspect the '[aisop-webapp](https://gitlab.com/aisop/aisop-webapp)' as an example. It uses the Node.js packagge "Passport" and its strategy [passport-oauth](https://www.passportjs.org/packages/passport-oauth/) to let students login and establish a session. The returned access tokens are stored in a CouchDB database and used when the app starts a web service request.

## Known bugs

When you open the oauth-route (e.g. http://localhost:3000/oauth) you will be redirected to Mahara. There you will need to log in with your username and password (if you haven't  done so before and bis this do not have an active Mahara session yet). After logging in you might receive the error "Unknown request". You can work around this error by logging into Mahara in advance and only then using this app. This is not a problem of this app but rather of Mahara which misses either a route or makes a wrong redirect. However, we haven't further inspected this problem yet. One idea could be changing the session handler to [memcached or redis](https://manual.mahara.org/en/18.04/administration/config_php.html#sessionhandler-select-the-session-handler).