/**
 * A sample app showing the OAuth 1.0a dance with Mahara.
 * Author: Alexander Gantikow
 * Projekt: AISOP. https://aisop.de/
 * Repository: https://gitlab.com/aisop/aisop-oauth
 * 
 * Mahara PHP example "REST with OAuth authentication"
 * https://wiki.mahara.org/wiki/Plugins/Auth/WebServices#REST_with_OAuth_authentication
*/

// ADD DETAILS OF YOUR EXTERNAL APP HERE!
const OAUTH_CONSUMER_KEY = 'enterConsumerKeyFromExternalApp';
const OAUTH_CONSUMER_SECRET = 'enterConsumerSecretFromExternalApp';

// CONFIGURE YOUR MAHARAS' OAUTH ENDPOINTS
// Replace 'your-domain.com' by your domain. Make sure it uses https.
const request_token_uri = 'https://your-domain.com/webservice/oauthv1.php/request_token';
const authorize_uri = 'https://your-domain.com/webservice/oauthv1.php/authorize';
const access_token_uri = 'https://your-domain.com/webservice/oauthv1.php/access_token';
const server_uri = 'https://your-domain.com/webservice/rest/server.php';

// NO MORE CHANGES REQUIRED BELOW
const OAUTH_CALLBACK = 'http://localhost:3000/oauth';
const OAUTH_ALGORITHM = 'HMAC-SHA1';

var express = require('express');
var router = express.Router();

// Require axios, retry interceptor and oauth interceptor
// Create Axios instance with an extendable config
const axios = require('axios').default;
const axiosRetry = require('axios-retry');
const { default: addOAuthInterceptor } = require("axios-oauth-1.0a");
const axiosClient = axios.create();

// Configure retry interceptor
axiosRetry(axiosClient, { 
	retries: 3,
	retryDelay: axiosRetry.exponentialDelay
});

// Specify the OAuth options
const oauthOptions = {
	algorithm: OAUTH_ALGORITHM,
	key: OAUTH_CONSUMER_KEY,
	secret: OAUTH_CONSUMER_SECRET,
	callback: OAUTH_CALLBACK
};

// TODO: REPLACE THIS IN YOUR IMEPLEMENTATION BY DATABASE
var readline = require('readline');
var rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

// Function sends post request to the mahara server to receive either a request or access token
// This looks like a simple post request, however, OAuthInterceptor is signing it in background
// Returns URLSearchParams object, see developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
async function postRequest(uri, data=undefined) {
	try {
		let response = await axiosClient.post(uri, data);
		return new URLSearchParams(response.data);
	} catch (error) {
		if (error.response) {
			// The request was made and the server responded with a status code
			// that falls out of the range of 2xx
			console.log(error.response.data);
			console.log(error.response.status);
			console.log(error.response.headers);
		} else if (error.request) {
			// The request was made but no response was received
			console.log(error.request);
		} else {
			// Something happened in setting up the request that triggered an Error
			console.log('Error', error.message);
		}
		return false;
	}
}

router.get('/', async function(req, res) {

	// Access the 'oauth_token' and 'oauth_verifier' query parameters
	// Only present when redirected back from mahara
	let oauth_token = req.query.oauth_token;
	let oauth_verifier = req.query.oauth_verifier;

	// If we don't have an user-accepted request token yet: go and get one.
	if (!oauth_token && !oauth_verifier) {
		console.log('----- Ask Mahara for request token -----');
		
		// Add axios interceptor for signing the following request
		let interceptor = addOAuthInterceptor(axiosClient, oauthOptions);

		// Ask for a request token; receive URLSearchParams object
		let params = await postRequest(request_token_uri);
		console.log("Mahara sent this request token:");
		console.log(params);

		// Remove axios interceptor
		axiosClient.interceptors.request.eject(interceptor);

		// Redirect user to Mahara to authorize the oauth_token
		// Append the request token to the redirect URI
		if (params) {
			let requestToken = params.get('oauth_token');
			res.redirect(`${authorize_uri}?oauth_token=${requestToken}`);
		} else {
			res.render('oauth', { feedback: "fail" });
		}
	} 
	
	// The user authorized the request token and was redirected back to aisop
	// The URL query now contains an oauth_token and oauth_verifier 
	// Tip: We extract both of them in code line 88 and 89 
	// Now we exchange the request token for an access token
	else {
		console.log('----- Get access token -----');
		console.log("Returned from Mahara with this data:");
		console.log(`oauth_token: ${oauth_token}, oauth_verifier: ${oauth_verifier} \n`);

		// IMPORTANT: here, we get the oauth_token_secret from a comand line input
		// This is not sufficient for a live system. This is why you will have to replace
		// this solution by a database. See https://gitlab.com/aisop/aisop-oauth for details.
		rl.question("Please copy and paste the oauth_token_secret from above: ", async function(oauth_token_secret) {
			rl.close();

			// Extend the OAuth options
			oauthOptions.token = oauth_token;					// from query
			oauthOptions.verifier = oauth_verifier;			// from query
			oauthOptions.tokenSecret = oauth_token_secret;	// from user input
			
			// Add axios interceptor for access token with updated options
			interceptor = addOAuthInterceptor(axiosClient, oauthOptions);

			// Get an access token from Mahara
			let params = await postRequest(access_token_uri);
			console.log("\nMahara sent this access token:");
			console.log(params);

			// Remove axios interceptor
			axiosClient.interceptors.request.eject(interceptor);

			// GOOD! We now own an access token! 
			// Let's use it to sign a webservice request.
			console.log("\n----- Using the access token to sign a request -----");

			let newOauthOptions = {
				algorithm: OAUTH_ALGORITHM,
				key: OAUTH_CONSUMER_KEY,
				secret: OAUTH_CONSUMER_SECRET,
				callback: OAUTH_CALLBACK,
				token: params.get('oauth_token'),
				tokenSecret: params.get('oauth_token_secret')
			};

			// Add axios interceptor for webservice access
			interceptor = addOAuthInterceptor(axiosClient, newOauthOptions);

			// Use webservice to get the extended context of the authenticated user
			let result = await postRequest(server_uri, {wsfunction: 'mahara_user_get_extended_context'});
			console.log(result);

			// Remove axios interceptor
			axiosClient.interceptors.request.eject(interceptor);

			res.render('oauth', { feedback: "Success. Go to your console to inspect the result" });
		});
	}
});

module.exports = router;