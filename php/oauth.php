<?php

include_once "oauth/library/OAuthStore.php";
include_once "oauth/library/OAuthRequester.php";

$usr_id = 0;
$options = array(
	'consumer_key' => 'dc0bd6c43f5fa2751180093be59894c60639c3646',
	'consumer_secret' => '84a5100ed51f77563b60ee4bb669ad56',
	'server_uri' => 'https://mahara.aisop.de/webservice/rest/server.php',
	'request_token_uri' => 'https://mahara.aisop.de/webservice/oauthv1.php/request_token',
	'authorize_uri' => 'https://mahara.aisop.de/webservice/oauthv1.php/authorize',
	'access_token_uri' => 'https://mahara.aisop.de/webservice/oauthv1.php/access_token',
);

// Note: do not use "Session" storage in production. Prefer a database
// storage, such as MySQL.
$store = OAuthStore::instance("Session", $options);

try {
	if (empty($_GET["oauth_token"])) {
		//  STEP 1:  If we do not have an OAuth token yet, go get one
		error_log('----- Get request token -----');
		$getAuthTokenParams = array(
			'xoauth_displayname' => 'Oauth test',
			'oauth_callback' => 'https://localhost/mahara/oauth.php'
		);
		// get a request token
		$tokenResultParams = OAuthRequester::requestRequestToken($options['consumer_key'], $usr_id, $getAuthTokenParams);
		error_log("tokenResultParams: ".var_export($tokenResultParams,1));

		// redirect to the Mahara authorization page, they will redirect back
		error_log('----- Redirect user to Mahara -----');
		$location = $options['authorize_uri'] . "?oauth_token=" . $tokenResultParams['token'];
		error_log("location: ".var_export($location,1));
		header("Location: " . $location);
	}
	else {
		//  STEP 2:  Get an access token
		error_log('----- Get access token -----');
		$oauthToken = $_GET["oauth_token"];
		$tokenResultParams = $_GET;  // why?
		error_log("oauth_token: ".var_export($oauthToken,1));
		error_log("tokenResultParams: ".var_export($tokenResultParams,1));

		try {
			error_log("trying to receive access_token");
			OAuthRequester::requestAccessToken($options['consumer_key'], $oauthToken, $usr_id, 'POST', $tokenResultParams);
		}
		catch (OAuthException2 $e) {
			// Something wrong with the oauth_token.
			error_log("Something wrong with the oauth_token.");
			var_dump($e);
			return;
		}
		 
		// Get secrets: consumer_key, consumer_secret, signature_methods, token; Params: $uri, $user_id
		// But why? $Secrets are not used in the code, also not in google example
		$secrets = $store->getSecretsForSignature(null, 1); 
		error_log("secrets/access_token: ".var_export($secrets,1));

		error_log('----- REST request -----');
		//$body = '{"wsfunction":"mahara_user_get_users_by_id","users":[{"id":'.$usr_id.'}]}';			// mahara original
		$body = '{"wsfunction":"mahara_user_get_context"}';
		//$request = new OAuthRequester($options['server_uri'].'?alt=json', 'POST', array(), $body);	// mahara original
		$request = new OAuthRequester($options['server_uri'], 'GET', $secrets, $body);				// google example
		error_log("request obj: ".var_export($request,1));

		/*
		$request = new OAuthRequester(  // Construct a new request signer. 
			$options['server_uri']."?oauth_timestamp=".$oauth_timestamp,     // the url
			'GET',                      // http method.  GET, PUT, POST etc.
			$tokenResultParams,         // name=>value array with request parameters
			$body,                      // optional body to send
			//null                      // optional files to send
		);
		*/

		$result = $request->doRequest($usr_id);
		if ($result['code'] == 200) {
			error_log("result: ".var_export($result,1));
			error_log("result body: ".var_export($result['body'],1));
		}
		else {
			echo 'Error';
		}
	}
}
catch(OAuthException2 $e) {
	echo "OAuthException:  " . $e->getMessage();
}

/*
*/
?>