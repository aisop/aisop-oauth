// i18n - internationalization 
const i18next = require('i18next');								// internationalization framework
const i18nextBackend = require('i18next-fs-backend');			// loads translations from file system
const i18nextMiddleware = require('i18next-http-middleware');	// middleware used with Node.js web frameworks

// Init express app
const express = require('express');
const app = express();
const port = 3000;

// Configure i18next obj
i18next
	.use(i18nextBackend)
	.use(i18nextMiddleware.LanguageDetector)
	.init({
		fallbackLng: 'de',
		saveMissing: true,
		preload: ['en', 'de'],
		//debug: true,
		backend: {
			// ns stands for namespace and is 'translation' by default
			loadPath: __dirname + '/locales/{{lng}}/{{ns}}.json',
			addPath: __dirname + '/locales/{{lng}}/{{ns}}.json'
		},
	});

// Tell Express to use i18next's middleware
app.use(i18nextMiddleware.handle(i18next));

// Static Files
app.use(express.static('public'));

// View-engine setup for 'ejs'
app.set('views', './views');
app.set('view engine', 'ejs');

// Require and use routes
app.use('/', require('./routes/index'));
app.use('/oauth', require('./routes/oauth'));

app.listen(port, () => 
	console.info(`App listening on port ${port}`)
);